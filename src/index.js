import riot from 'riot'
import 'riot-hot-reload'
import './app/app.tag'
import route from 'riot-route/lib/tag'
import store from './store'

import './styles/main.scss'

route.base('/')
riot.mount('app', { store })

import erre from 'erre'
import { mergeDeepRight } from 'ramda'

import teams from './entities/teams/data'
import profile from './entities/profile/data'

let store = {
  teams,
  profile
}

export const storeStream = erre(val => {
  store = mergeDeepRight(store, val)
  return mergeDeepRight(store, val)
})

export const getState = prop => store[prop]

export default store

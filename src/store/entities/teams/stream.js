import erre from 'erre'
import { storeStream, getState } from '../../index'
import { teamsReducers } from './reducer'

export const teamsStream = erre(action => teamsReducers(getState('teams'), action))
teamsStream.on.value(teams => storeStream.push({ teams }))

<team>
  <h1>This is team {team.name}</h1>
  <button onclick={challengeTeam}>Challenge {team.name}</button>
  <a href="/">Go back</a>
  
  <script>
    import { equals } from 'ramda'
    import route from 'riot-route'
    import { challengeTeam } from '../../store/entities/profile/actions'

    this.team = {}
    this.teamId = null

    this.on('route', id => {
      this.team = opts.teams[id]
      this.teamId = id
    })

    this.shouldUpdate = (data, nextOpts) => !equals(this.opts, nextOpts)

    this.challengeTeam = () => {
      challengeTeam(this.teamId, this.opts.profile.team)
      route('/')
    }
  </script>
</team>

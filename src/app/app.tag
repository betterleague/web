<app>
  <h1>BetterLeague ranking</h1>
  <router>
    <route path="team/*">
      <team
        teams={parent.parent.store.teams}
        profile={parent.parent.store.profile} />
    </route>

    <route path="/">
      <matches
        profile={parent.parent.store.profile}
        matches={parent.parent.store.profile.matches}
        teams={parent.parent.store.teams} />
      <challenges
        profile={parent.parent.store.profile}
        challenges={parent.parent.store.profile.challenges}
        teams={parent.parent.store.teams} />
      <ranking-table
        teams={parent.parent.store.teams}
        profile={parent.parent.store.profile} />
    </route>
  </router>
  
  <script>
    import './team/team.tag'
    import './ranking-table/ranking-table.tag'
    import './challenges/challenges.tag'
    import './matches/matches.tag'
    import { storeStream } from '../store'
    this.store = opts.store

    storeStream.on.value(val => {
      this.store = val
      this.update()
    })
  </script>
</app>
